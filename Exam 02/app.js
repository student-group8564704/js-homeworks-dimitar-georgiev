/*
For Exam 02 you need to create a Simple Books Store like the one on the image. You need to write two constructor functions. The first one is BookStore with the following properties(name, books, shoppingCard, wishlist) and the following methods (addBook, listBooks, addToShoppingCard, addToWishlist, removeFromShoppingCard, removeFromWishlist). The only parameter you need in the BookStore constructor function is the name.

The second constructor function is Book with the following properties (title, author, image, price, quantity, description).

Create the BookStore and create some Books. The required methods in the BookStore should all be functional.

When the Buy button is clicked the Book should be added to Shopping card.

When the Add To Wishlist button is clicked the Book should be added to Wishlist.

The Remove buttons should remove the book from the list where it is shown through the removeFromShoppingCard and removeFromWishlist methods.

Bonus: If a book is added to the Shopping card the quantity of the book should decrese by 1.

If a book has 0 Quantity then there shouldn't be a Buy and Add To Wishlist buttons for that book.

*/

function BookStore(name) {
  this.name = name;
  this.books = [];
  this.shoppingCart = [];
  this.wishlist = [];

  this.addBook = function(book){
    this.books.push(book);
  };

    //listing the added books
  this.listBooks = function(){
    const bookList = document.querySelector('#books-list');

    let htmlToAdd = "";

    let index = 0;

    const actionBtns = `<button id="buy-btn">Buy</button>
                      <button id="wish-btn">Wishlist</button>`;

    for(let book of this.books){
      htmlToAdd += `
        <li class="card" data-index="${index}">
          <div class="card-title">${book.title}</div>
          <div class="card-img"><img src="${book.image}"></div>
          <div class="card-description">${book.description}</div>
          <div class="card-price">Price: ${book.price}</div>
          <div class="card-quantity">Quantity: ${book.quantity}</div>
          <div class="card-actions">
            ${actionBtns}
          </div>
      `
      index++;
    };
    bookList.innerHTML = htmlToAdd;
  };




//listing books in shopping cart

  this.addToShoppingCart = function(book){
    this.shoppingCart.push(book);
  }

  this.listShoppingCart = function(){
    const shoppingCartList = document.querySelector('#shopping-cart');

    let htmlToAdd = "";

    let shopIndex = 0;

    const removeBtn = `<button class="remove">Remove</button>`;

    for(let book of this.shoppingCart){
      htmlToAdd +=`
      <li data-index="${shopIndex}"> ${book.title} ${book.author} ${book.price} ${removeBtn}</li>
      `
    }
    shoppingCartList.innherHTML = htmlToAdd;

    
  }



}



function Book(title, author, image, price, quantity, description){
  this.title = title;
  this.author = author;
  this.image = ""
  this.price = price;
  this.quantity = quantity;
  this.description = description;
}







const myBookStore = new BookStore("SEDC");

const addBtn = document.querySelector("#add-btn");
addBtn.addEventListener("click", function (e) {
  e.preventDefault();

  const title = document.querySelector('#title').value;
  const author = document.querySelector('#author').value;
  const image = document.querySelector('#thumbnail').value;
  const price = document.querySelector('#price').value;
  const quantity = document.querySelector('#quantity').value;
  const description = document.querySelector('#description').value;


  myBookStore.addBook(new Book(title, author, image, price, quantity, description));
  myBookStore.listBooks();
});


let buyBtn = document.getElementById("#buy-btn");
console.log(buyBtn);
buyBtn.addEventListener("click", function (e){
  e.preventDefault();
  let buyIndex = parseInt(e.target.parentNode.getAttribute("index"));
  console.log(buyIndex);

  myBookStore.addToShoppingCart(myBookStore.books[buyIndex]);
  myBookStore.listBooks();
});

