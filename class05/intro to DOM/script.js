
//GET ELEMENTS BY ID
let myHeader = document.getElementById("myTitle");
console.log(myHeader);
console.log(myHeader.innerText);

//GET ELEMENTS BY CLASS
let paragraphs = document.getElementsByClassName("myParagraph");
let secondParagraph = document.getElementsByClassName("second");

console.log(paragraphs);
console.log(paragraphs[0].innerText);
console.log(secondParagraph);
console.log(secondParagraph[0]);

//GET ELEMENTS BY TAG NAME
let paragraphsByTagName = document.getElementsByTagName("p");
console.log(paragraphsByTagName);
console.log(paragraphsByTagName[1]);

let texts = document.getElementsByTagName("text");
console.log(texts);
console.log(texts[0]);

//GET ELEMENTS WITH QUERY SELECTORS
let paragraphsWithQuerySelectors = document.querySelectorAll("p");
console.log(paragraphsWithQuerySelectors);
console.log(paragraphsWithQuerySelectors[0]);

let firstP = document.querySelector(".myParagraph");
console.log(firstP);

let header = document.querySelector("#myTitle");
console.log(header);

//FINDING SIBLING ELEMENTS

let para = document.getElementsByClassName("myParagraph")[0];
let sibling = para.previousElementSibling;
let paraSecond = document.getElementsByClassName("myParagraph")[1];
let sibling2 = paraSecond.nextElementSibling;
console.log(para);
console.log(sibling);
console.log(sibling2);


//FINDING PARENT ELEMENTS
let paragraph = document.getElementsByClassName("myParagraph")[0];
let parentDiv = paragraph.parentElement;
console.log(paragraph);
console.log(parentDiv);

//FINDING CHILD ELEMENTS
let myDiv = document.getElementById("main");
let divChildren = myDiv.children; //ALL CHILDREN
console.log(divChildren);

let divFirstChild = myDiv.firstElementChild;
console.log(divFirstChild);

let divLastChild = myDiv.lastElementChild;
console.log(divLastChild);


//GETTING TEXT
let textSpaces = myDiv.textContent;
console.log(textSpaces);
let textNoSpaces = myDiv.innerText;
console.log(textNoSpaces);

//CHANGINGTEXT IN AN ELEMENT

console.log(header.innerText);  //checking what is the text
header.innerText = "";  //changing the text to nothing
console.log(header.innerText);

header.innerText = "New TEXT!";
console.log(header.innerText);

header.innerText += " Ultra new text";
console.log(header.innerText);

//CHANGING AND ADDING CODE

console.log(myDiv);
//adding new element in the div

myDiv.innerHTML += `<p class="new">
Paragraph generated from JavaScript
</p>`

console.log(myDiv);

// myDiv.innerHTML = "";//removing all code from myDiv;
// console.log(myDiv);