let firstDiv = document.getElementById('first');
console.log(firstDiv);

let allParagraphs = document.getElementsByTagName("p");
console.log(allParagraphs);

let lastDiv = document.getElementsByTagName("div")[2];
console.log(lastDiv);

let lastHeader = lastDiv.lastElementChild;
console.log(lastHeader);

let firstHeader = lastDiv.firstElementChild;
console.log(firstHeader);

let firstParagraph = document.getElementsByClassName("second")[0];
// let firstParagraphText = firstParagraph.innerText;
console.log(firstParagraph.innerText);



let textElement = document.getElementsByTagName("text")[0];
textElement.innerText += " text";
console.log(textElement.innerText);

lastHeader.innerText = "This is also changed";

firstHeader.innerText = "It is changed";