// function myFunction(){
//   // loops, if statements, anything...
//   console.log("Hello, World");

// }

// myFunction();
// myFunction();
// myFunction();

// //Functions with parameters

// function myFunctionWithParameters(x,y){
//   var myVar = x*y;
//   console.log(myVar);
// }

// myFunctionWithParameters(1,3)
// myFunctionWithParameters(2,5);
// myFunctionWithParameters(23,-56);


// //Returning Functions

// function myFunctionWithReturn(x,y){
//   let myVar = x*y;
//   return myVar;

// }

// myFunctionWithReturn(1,3);

// let result = myFunctionWithReturn(5,10);
// console.log(result);

// function fullName(firstName, lastName){
//   return firstName + " " + lastName;
// }

// let firstName = "Petar";
// let lastName = "Pavlovski";

// let fullNameResult = fullName(firstName, lastName);
// let fullNameResult1 = fullName(lastName, firstName);
// console.log(fullNameResult);
// console.log(fullNameResult1);

function sum2num(x,y){
  var varSum = x+y;
  return varSum;
}

var sum = sum2num(3,5);
console.log(sum);

//THE TEMPERATURE CONVERTER

function celsiusToFahrenheit(temp){
  let fahrenheit = (temp*9)/5 + 32;
  return `${temp}C is ${fahrenheit}F`
}

console.log(celsiusToFahrenheit(16));


function fahrenheitToCelsius(temp){
  let celsius = ((temp-32)*5)/9;
  return `${temp}F is ${celsius}C`
}

console.log(fahrenheitToCelsius(85));


function tellFortune(numOfChildren, partnerName, location, jobTitle){
  return `You will be a ${jobTitle} in ${location}, and married to ${partnerName} with ${numOfChildren} kids.`
}

console.log(tellFortune(2, "Tijana", "Scotland", "nadezen developer"));
console.log(tellFortune(3, "Tijana", "Canada", "nadezen developer"));
console.log(tellFortune(4, "Tijana", "L.A.", "nadezen developer"));

//CONTROL STRUCTURES

// let number = 14;

// if(typeof number == "number"){
//   console.log("the variable is a number");
// } 

// let score = 10;

// if(score>100){
//   alert ("you won");
// }
// else if(score<100){
//   alert ("you lose");
// }
// else{
//   alert ("it's a tie")
// }

// var fridayCash = +prompt("What's your friday night budget?");

// if(fridayCash>=1000){
//   console.log("You have a solid amount for a good night");
// }
// else if(fridayCash>=500){
//   console.log("I mean... not great, not terrible");
// }
// else if(fridayCash>=200){
//   console.log("I guess you can get like a drink or two... max");
// }
// else{
//   console.log("Just stay home, man...");
// }

// function greaterNum(x,y){
//   var bigger;
//   if(x>y){
//     bigger=x;
//   }
//   if(x<y){
//     bigger=y;
//   }
//   return `of the numbers ${x} and ${y}, the bigger number is ${bigger}`;
// }

// console.log(greaterNum(2,5));
// console.log(greaterNum(8,5));

// function assignGrade(x){
//   if(x>80){
//     return `You've earned an A`;
//   }
//   else if(x>60){
//     return `You've earned a B`;
//   }
//   else if(x>40){
//     return `You've earned a C`;
//   }
//   else if(x>20){
//     return `You've earned a D`;
//   }
//   else{
//     return `You failed, you have an F`
//   }

// }

// var points = +prompt("How many points did you get?");

// console.log(assignGrade(points));

// function helloWorld(language) {

//   if (language == 'Spanish') {
//     return 'Hola';
//   }
//   else if (language == 'Italian') {
//     return 'Ciao';
//   }
//   else if (language == 'Macedonian') {
//     return 'Zdravo'
//   }
//   else if (language == 'Deutsch') {
//     return 'Sonne'
//   }
//   else {
//     return 'Hi'
//   }
  
//   }
  
//   console.log(helloWorld('Spanish'));
//   console.log(helloWorld('Italian'));
//   console.log(helloWorld(''));

function chineseZodiac(year){
  let result = (year-4)%12;

  switch(result){

      case 0:
        console.log("Rat");
        break;
      case 1:
          console.log("Ox");
          break;
      case 2:
        console.log("Tiger");
        break;
      case 3:
        console.log("Rabbit");
        break;
      case 4:
        console.log("Dragon");
        break;
      case 5:
        console.log("Snake");
        break;
      case 6:
        console.log("Horse");
        break;
      case 7:
        console.log("Goat");
        break;
      case 8:
        console.log("Monkey");
        break;
      case 9:
        console.log("Rooster");
        break;
      case 10:
        console.log("Dog");
        break;
      case 11:
        console.log("Pig");
        break;
  }
}

chineseZodiac(1995);