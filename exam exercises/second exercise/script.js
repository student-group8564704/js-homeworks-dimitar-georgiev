

function sizeUpdate(){
  var width = window.innerWidth;
  var height = window.innerHeight;
  document.getElementById("height").innerText = height;
  document.getElementById("width").innerText = width;
}

window.addEventListener('resize', sizeUpdate);