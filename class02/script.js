let unknown = null;

let studentName = "Damjan";
let studentSurname = "Petrovski";
let birthDate = 2001;
let car;

let residence = {
  opstina: "Aerodrom",
  grad: "Skopje",
  drzava: "Makedonija",
}

let boolean = true;

console.log(unknown);
console.log(studentName + " " + studentSurname);
console.log(birthDate);
console.log(unknown);
console.log(car);
console.log(residence);
console.log(boolean);


// concatenation

console.log("SEDC" + " " + "2019");

let result = "42" - 20;
console.log(result);

let result1 = "Hello" - 42;
console.log(result1);

const sedc = "SEDC";

console.log(sedc + " " + 2018);

let result2 = `Hello ${sedc} Academy 2018`;
console.log(result2);

let year = 2024;
let result3 = `Copyright ${year} SEDC Academy`;
console.log(result3);

let text = 'it\'s really nice to be a programmer';
console.log(text);

// compparison operators

// let a = 5;
// let b = 10;
// let c = 10;

// console.log(a>b);
// console.log(a>=b);
// console.log(a<b);
// console.log(c<=b);

// let d = "5";
// let e = "Hello";
// let f = "Hello";

// console.log(a == b);
// console.log(a == d);
// console.log(a == e);

// console.log(a != b);
// console.log(a === b);

// console.log(d!==e);
// console.log(f!==e);

// LOGICAL OPERATORS

// let expr1 = true;
// let expr2 = false;
// let expr3 = true;
// let expr4 = false;

// console.log(expr1 && expr2);
// console.log(expr1 && expr3);

// console.log(expr1 || expr2);
// console.log(expr1 || expr3);
// console.log(expr2 || expr4);


// console.log(!expr1);

// STRUCTURE

// let pass = 50;
// let score = 90;
// let hasPassed = score >= pass;

// console.log(hasPassed);

// logical AND (&&) 

// let a1 = true && true;
// console.log(a1);

// let a2 = true && false;
// console.log(a2);

// let a3 = false && true;
// console.log(a3);

// let a4 = false && (3 == 4);
// console.log(a4);

// let a8 = "" && false;
// console.log(a8);

// let a9 = false && "";
// console.log(a9);


// logical OR

// let o1 = true || true;
// console.log(o1);

// let o2 = false || true;
// console.log(o2);

// let o3 = true || false;
// console.log(o3);

// let o4 = false || (3 == 4);
// console.log(o4);

// let o8 = "" || false;
// console.log(o8);

// let o9 = false || "";
// console.log(o9);


// logical NOT

// let n1 = !true;
// let n2 = !false;
// let n3 = !"Cat";
// console.log(n3);

// == OR ===

// let dilema = 0 == false;
// console.log(dilema);

// let dilema1 = 0 === false;
// console.log(dilema1);


// INEQUALITY

// let a = 41;
// let b = "42";
// let c = "43";

// console.log(a<b);
// console.log(b<c);


// let r = 42;
// let k = "foo";

// console.log(r>k);
// console.log(r<k);
// console.log(r==k);