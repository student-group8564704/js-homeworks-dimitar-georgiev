const apiUrl = "https://api.openweathermap.org/data/2.5/weather?units=metric&appid=a3cc0ca767437dee530ff93eda071bd8&q=";

const searchBtn = document.querySelector("#searchBtn");

async function checkWeather(){

  const searchInput = document.querySelector("#searchField").value;
  
  const weatherIcon = document.querySelector("#weatherIcon");
  const temp = document.querySelector("#temp");
  const cityName = document.querySelector("#cityName");
  const humidity = document.querySelector("#humidityValue");
  const wind = document.querySelector("#windSpeed");

  document.querySelector(".results").style.display = "block";

  const response = await fetch(apiUrl + searchInput);

  var data = await response.json();

  console.log(data);

  cityName.textContent = data.name + "   -   " + data.weather[0].main;
  temp.textContent = Math.round(data.main.temp) + "°C";
  humidity.textContent ="Humidity: " + Math.round(data.main.humidity) + "%";
  wind.textContent ="Wind speed: " + Math.round(data.wind.speed) + "km/h";

  console.log(data.weather[0].main);
  if(data.weather[0].main == "Clouds"){
    weatherIcon.src = "./images/icons/cloudy.png"
  }
  else if(data.weather[0].main == "Clear"){
    weatherIcon.src = "./images/icons/clear.png"
  }
  else if(data.weather[0].main == "Mist" || "Smoke" || "Haze" || "Dust" || "Fog" || "Sand" || "Ash" || "Squall" || "Tornado"){
    weatherIcon.src = "./images/icons/smog.png"
  }
  else if(data.weather[0].main == "Snow"){
    weatherIcon.src = "./images/icons/snowing.png"
  }
  else if(data.weather[0].main == "Rain"){
    weatherIcon.src = "./images/icons/rain.png"
  }
  else if(data.weather[0].main == "Drizzle"){
    weatherIcon.src = "./images/icons/drizzle.png"
  }
  else if(data.weather[0].main == "Thunderstorm"){
    weatherIcon.src = "./images/icons/thunderstorm.png"
  }
  
}


searchBtn.addEventListener("click", checkWeather);

