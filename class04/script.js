// // //ARRAYS

// // let colors = ["red", "blue", "green", "purple", "yellow"]
// // console.log(colors[0]);
// // console.log(colors[3]);

// // //Creating an array
// // let days = ["Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"];
// // console.log(days[6]);

// // //empty array
// // let iAmEmptyArray = [];
// // console.log(iAmEmptyArray[0]);

// // //mixed array
// // let iAmMixedArray = [1, 2, "i am string", false];
// // console.log(iAmMixedArray[1]);
// // console.log(iAmMixedArray[2]);
// // console.log(iAmMixedArray[3]);


// // let authors = [
// //   "Ernest Hemingway",
// //   "Charlotte Bronte",
// //   "Dante Alighieri",
// //   "Emily Dickinson"
// // ]
// // console.log(authors);

// // //accessing specific item in array
// // console.log(days[1]);

// // days[1] = "Monday";
// // console.log(days);

// // console.log(days.length);

// // days[days.length] = "No more days in the week";
// // console.log(days);

// // days.push("first push", "second push", "third push");
// // console.log(days);


// // //adding element to beginning of array
// // let properties = ["red", "14px", "Arial"];
// // properties.unshift("bold", "italic", "underlined");
// // console.log(properties);


// // //deleting items in array

// // let p = [0, 1, 2, 3];
// // let removedItem = p.pop();
// // console.log(removedItem);

// // let removedItemFromBeginning = p.shift();
// // console.log(removedItemFromBeginning);


// // //adding element at specific position/index with splice method

// // let fruits = ["banana", "orange", "apple", "mango"];
// // fruits.splice(2, 0, "Lemon", "Kiwi");
// // console.log(fruits);

// // let task1 = ["2","4","6","8","10"];

// // console.log(task1[1]);

// // console.log(task1[task1.length-1]);

// // task1.push("posleden");

// // console.log(task1.length);

// // task1.splice(2, 0, "nov1");

// // let specificElement = task1[1];
// // task1.push(specificElement);
// // task1.splice(1, 1, "posleden nov");
// // console.log(task1);



// // LOOPS

// // let condition = 10;

// // while(condition <= 20){
// //   //javascript to repeat
// //   console.log(condition);
// //   document.write(condition + "<br>");
// //   condition++;
// // }


// // //while with arrays

// let denovi = ["ponedelnik", "vtornik", "sreda", "cetvrtok", "petok", "sabota", "nedela"];
// // let counter = 0;

// // while (counter<denovi.length){
// //   document.write(denovi[counter] + ", ");
// //   counter++;
// // }

// // let niza = [2, 4, 66, 8, 12, 5, 1, 3, 29, 5];

// // let counter = 0;
// // var largest = niza[0];

// // while (counter < niza.length){
  
// //   if (largest<niza[counter]){
// //     largest = niza[counter];
// //   }
// //   counter++;
// // }
// // console.log(largest);



// // do/while loops

// // let conditionTwo = 1;

// // do {

// //   console.log(conditionTwo);
// //   conditionTwo++
// // }
// // while(conditionTwo<10);


// //FOR loops

// // let num = [233, 40, 50, 145];

// // for(let i=0; i < num.length; i++){
// //   console.log(num[i]);
// // }

// // let num1 = 1;

// // while(num1 <= 100) {
// //   console.log("Number " + num1);
// //   num1 += 1;
// // }

// // for (let num2 = 1; num2 <= 100; num2++){
// //   document.write("Number " + num2 + "<br>");
// // }

// // FOR WITH ARRAYS

// for(let i = 0; i < denovi.length; i++){
//   console.log(denovi[i] + ", ")

// }

// for(let i = 60; i<=100; i++){
//   if(i<70){
//     console.log("For " + i + " points, you got a C");
//   }
//   else if(i<90){
//     console.log("For " + i + " points, you got a B");
//   }
//   else if(i<=100){
//     console.log("For " + i + " points, you got a A");
//   }
// }

let bodovi = [72,99,60,73,92,87];

for(let i=0; i<bodovi.length; i++){
  if(bodovi[i]<70){
        console.log("For " + bodovi[i] + " points, you got a C");
      }
  else if(bodovi[i]<90){
        console.log("For " + bodovi[i] + " points, you got a B");
      }
  else if(bodovi[i]<=100){
        console.log("For " + bodovi[i] + " points, you got a A");
      }

}