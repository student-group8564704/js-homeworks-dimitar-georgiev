function hide(){
  alert("The hide function is executed")
}

// let button = document.querrySelector(".first-btn");

// button.onClick = hide;


function countRabbits() {
  for(let i=1; i<=3; i++){
    alert("Rabbit number " + i);
  }

}

let inputUser = document.querySelector(".inputUser");
let inputBtn = document.querySelector(".input-btn");

function checkUsername(){
  //code to check the length of a username
  if(inputUser.value == "admin"){
    alert("Welcome admin!");
  }
  else{
    alert("Wrong user.");
  }
}

// inputBtn.onclick = checkUsername;


const elem = document.getElementById("btnHello");

// elem.onclick = function(){
//   alert("Hello world");
// }

function sayHello(){
  alert("hello World");
}

// elem.onclick = sayHello;   right way
// elem.onclick = sayHello();  wrong way, posto odma ja izvrsuva funkcijata, ne ceka onclick

function iWon(){
  alert("I'm the WINNER");
}

elem.onclick = iWon;

//EVENT LISTENER

// inputBtn.addEventListener("keypress", checkUsername, false);

inputUser.addEventListener("keypress", function(event){
  if(event.key === "Enter"){
    checkUsername();
  }
});

const inputUsername = document.querySelector("#username");

// inputUsername.addEventListener("blur", function(){
//   const greeting = `Hello ${inputUsername.value}`;
//   alert(greeting);
// })

function greetingFunc() {
  const greeting = `Hello ${inputUsername.value}`
  alert(greeting)
}

inputUsername.addEventListener("blur", greetingFunc);


inputUsername.addEventListener("blur", function(event){
  const greeting = `Hello ${event.target.value}`;
  alert(greeting);
})

const redElement = document.getElementById("redDiv");

function setColorToDiv(event) {
  event.target.style.backgroundColor = "red";
//moze i redElement.style.backgroundColor = "red";
}

redElement.addEventListener("mousemove", setColorToDiv);

redElement.removeEventListener("mousemove", setColorToDiv);


//USING PARAMETERS WITH EVENT HANDLERS AND LISTENERS

const elUsername = document.getElementById("usernameInput");

const elMsg = document.getElementById("feedback");

function checkUsernameInput(minLength) {

  if(elUsername.value.length < minLength){
    elMsg.textContent = `Username must be ${minLength} characters or more`;
  }
  else{
    elMsg.innerHTML = "";
  }
}

elUsername.addEventListener("blur", function(){
  checkUsernameInput(5);
  
},
false);

const styleBtn = document.getElementById("style-btn");
const styleHeader = document.getElementById("styleChange");

function styleChange(){
  styleHeader.style.backgroundColor= "red";
  styleHeader.style.color= "white";
  styleHeader.style.fontSize= "30px";
}

styleBtn.addEventListener("click", styleChange)