let firstChange = document.getElementById("myTitle");
console.log(firstChange.textContent);
firstChange.textContent = "Changed h1";


let secondChange = document.getElementsByClassName("paragraph")[0];
console.log(secondChange.textContent);
secondChange.textContent = "Changed first paragraph"


let thirdChange = document.getElementsByClassName("paragraph")[1];
console.log(thirdChange.textContent);
thirdChange.textContent = "Changed second paragraph";


let fourthChange = document.getElementsByTagName("text")[0];
console.log(fourthChange.textContent);
fourthChange.textContent = "Changed text";

let fifthChange = document.getElementsByTagName("h1")[1];
console.log(fifthChange.textContent);
fifthChange.textContent = "Second h1 change";


let finalChange = document.getElementsByTagName("h3")[0];
console.log(finalChange.textContent);
finalChange.textContent = "Final change for this homework";