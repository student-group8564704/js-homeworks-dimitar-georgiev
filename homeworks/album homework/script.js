// For this exercise you need to create an Album list like the one in the image. You should write two constructor functions. The first one is MusicAlbum with the following properties (name, artist, artistImage, songs, yearReleased, albumCover, albumDuration) and two methods (addSong) to add the songs to your album and the second method (listAllSongs) to display all songs on the screen just like in the image.

// The second constructor function is Song with the following properties (title, artist, plays, duration).

// After that create the Album and create a couple of Songs. Add the Songs to the Album and list all songs on the screen.

// Use the image from the Spotify album as a reference for the structure, but keep in mind that you can use an album and an artist of your choice if you like.

// Bonus(but not required): Style the HTML just like in the image.




function MusicAlbum (name, artist, artistImage, songs, yearReleased, albumCover, albumDuration) {

  this.name = name;
  this.artist = artist;
  this.artistImage = artistImage;
  this.songs = [];
  this.yearReleased = yearReleased;
  this.albumCover = albumCover;
  this.albumDuration = albumDuration;
  console.log(this.songs);





  this.listAlbum = function(){
    const albumInfo = document.querySelector("#album-name");

    let novhtml = "";

    novhtml += `
    <div id="album-cover">${this.albumCover}</div>
    <div id="cursed-album-info">
    <h4>Album</h4>
    <h1>${this.name}</h1>
    <div id="album-tiny-info">
      <div id="artist-image">${this.artistImage}</div>
      <h3>${this.artist}</h3>
      <h4>${this.yearReleased}</h4>
      <h4>${this.songs.length} songs</h4>
      <h4>${this.albumDuration}</h4>
    </div>
    `

    albumInfo.innerHTML += novhtml;
  }

  this.addSong = function(song){
    this.songs.push(song);
  }


  this.listAllSongs = function() {
    const topList = document.querySelector("#song-list");

    let html = "";

    html += `<li>
    <div class="number">#</div>
    <div class="title">Title</div>
    <div class="plays">Plays</div>
    <div class="duration">Duration</div>  
    </li>
    <hr>  
    `;

    
    let index = 1;
    
    for(let song of this.songs){
      html +=`<br>
      <li>
        <div class='number'>${index}</div>
        <div class='title'>${song.title}<br>${song.artist}</div>
        <div class='plays'>${song.plays}</div>
        <div class='duration'>${song.duration}</div>
      </li>
      `
      index++;
    }
    topList.innerHTML += html;


  }



}






function Song(title, artist, plays, duration) {
  this.title = title;
  this.artist = artist;
  this.plays = plays;
  this.duration = duration;
}

const armee = new Song("Armee der Tristen", "Rammstein", "7 867 119", "3:25");
const zeit = new Song("Zeit", "Rammstein", "17 928 719", "5:21");
const schwartz = new Song("Schwartz", "Rammstein", "8 628 927", "4:18");
const giftig = new Song("Giftig", "Rammstein", "19 827 019", "3:08");
const zick = new Song("Zick Zack", "Rammstein", "13 928 029", "4:04");
const ok = new Song("OK", "Rammstein", "6 829 029", "4:03");
const meine = new Song("Meine Tranen", "Rammstein", "7 829 829", "3:57");
const angst = new Song("Angst", "Rammstein", "27 920 817", "3:44");
const dicke = new Song("Dicke Titten", "Rammstein", "13 028 651", "3:38");
const lugen = new Song("Lugen", "Rammstein", "12 981 332", "3:49");
const adieu = new Song("Adieu", "Rammstein", "7 928 116", "4:39");



const rammstein = new MusicAlbum("Zeit", "Rammstein", "<img src='./images/till.jpg'>",  "", "2022",  '<img src="./images/zeit.png">', "1:23:11");
rammstein.addSong(armee);
rammstein.addSong(zeit);
rammstein.addSong(schwartz);
rammstein.addSong(giftig);
rammstein.addSong(zick);
rammstein.addSong(ok);
rammstein.addSong(meine);
rammstein.addSong(angst);
rammstein.addSong(dicke);
rammstein.addSong(lugen);
rammstein.addSong(adieu);

rammstein.listAlbum();
rammstein.listAllSongs();





