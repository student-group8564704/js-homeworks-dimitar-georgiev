function createTable(){
  let rowCount = +prompt("How many rows in your table?");
  let columnCount = +prompt("How many columns in your table?");

  let table = document.createElement("table");
  table.style.border="1px solid red";
  
  document.body.appendChild(table);

  for(let i = 1; i <= rowCount; i++){
    let newRow = document.createElement('tr');
      for (let j = 1; j <= columnCount; j++){
        let newColumn = document.createElement('td');
        newColumn.textContent = "Row-" + i + ", Column-" + j;
        newColumn.style.border="1px solid red";
        newColumn.style.padding="15px";
        newRow.appendChild(newColumn);
      }
      table.appendChild(newRow);
  }
  
}


createTable();