let day;
var day1;
const day2 = "Monday";
console.log(day2);

// console.log(day2);

// define a string variable

let text = "Task 01";
console.log(text);
console.log(typeof text);

// define a number variable

let num = 8;
console.log(num);
console.log(typeof num);

// undefined

let test;
console.log(test);
console.log(typeof test);

// boolean variable

let bool = true;
console.log(bool);
console.log(typeof bool);

// null variable
let number = null;
console.log(number);
console.log(typeof number);

// operators

let a = 5;
let b = 11;

console.log(a+b);
console.log(b-a);
console.log(b*a);
console.log(a/b);
console.log(3%2);


let c = 3;
c++;
console.log(c);


let x = 5;
let y = 2;
console.log(x==y);

// alert(x<=y);


// task 1
let ime = "Martin";
let year = 1994;
let text2 = ime + " " + year;
console.log(typeof text2);

let mentorForJS = true;
console.log(mentorForJS);

// ARRAY!!!

let cars = ["Mercedes", "BMW", "Bentley"];
console.log(cars);

// OBJECT!!!

let student = {
  ime: "Martin",
  year: 1994,
  car: "Opel",
}

console.log(student.ime);
console.log(student.year);
console.log(student.car);


let firstName = "Dimitar";
let lastName = "Georgiev";
console.log(firstName + " " + lastName); 


//Task 2
// let feet = +prompt("enter length in feet");
// let meters = feet*0.3048 + " meters";
// alert(meters);
// console.log(meters);


// Task 3
// let sideA = 3;
// let sideB = 4;
// console.log("the area is " + (sideA*sideB));


// Task 4
let r = 5;
const pi = 3.14
let area = r*r*pi
console.log("Area of circle is " + area);

